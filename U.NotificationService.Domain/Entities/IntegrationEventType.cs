namespace U.NotificationService.Domain.Entities
{
    public enum IntegrationEventType
    {
        Unknown,
        ProductPublishedIntegrationEvent,
        ProductPropertiesChangedIntegrationEvent,
        ProductAddedIntegrationEvent
    }
}