namespace U.NotificationService.Domain.Entities
{
    public enum Importancy
    {
        Trivial = 0,
        Normal = 1 ,
        Important = 2
    }
}