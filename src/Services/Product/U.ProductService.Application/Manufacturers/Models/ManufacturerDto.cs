namespace U.ProductService.Application.Manufacturers.Models
{
    public class ManufacturerDto
    {
        public string Name { get; set; }  
        public string Description { get; set; }
        public int? PictureId { get; set; }
    }
}