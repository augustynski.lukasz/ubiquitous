namespace U.SmartStoreAdapter.Application.Models.Categories
{
    public class CategoryViewModel : CategoryDto
    {
        public int Id { get; set; }
    }
}