namespace U.SmartStoreAdapter.Application.Models.Pictures
{
    public class PictureDto
    {
        public string SeoFileName { get; set; }
        public string MimeType { get; set; }
        public string Url { get; set; }
    }
}