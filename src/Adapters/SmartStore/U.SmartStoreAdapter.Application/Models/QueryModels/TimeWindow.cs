using System;

namespace U.SmartStoreAdapter.Application.Models.QueryModels
{
    public class TimeWindow
    {
        public DateTime TimeFrom { get; set; }
        public DateTime TimeTo { get; set; }
    }
}