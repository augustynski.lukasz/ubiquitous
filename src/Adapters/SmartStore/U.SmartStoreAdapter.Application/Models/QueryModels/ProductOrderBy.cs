// ReSharper disable UnusedMember.Global

namespace U.SmartStoreAdapter.Application.Models.QueryModels
{
    public enum ProductOrderBy
    {
        Id,
        ProductTypeId,
        ParentGroupedProductId,
        Name,
        FullDescription,
        Sku,
        ManufacturerPartNumber,
        Gtin,
        DownloadId,
        TaxCategoryId,
        StockQuantity,
        DisplayStockQuantity,
        MinStockQuantity,
        NotifyAdminForQuantityBelow,
        AllowBackInStockSubscriptions,
        OrderMinimumQuantity,
        OrderMaximumQuantity,
        Price,
        OldPrice,
        ProductCost,
        SpecialPrice,
        Weight,
        Length,
        Width,
        Height,
        DisplayOrder,
        SystemName,
        CreatedOnUtc,
        UpdatedOnUtc
    }
}