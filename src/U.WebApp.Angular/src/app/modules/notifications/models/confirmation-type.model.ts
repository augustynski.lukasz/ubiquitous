export enum ConfirmationType
{
  unread = 0,
  read = 1,
  hidden = 2
}
