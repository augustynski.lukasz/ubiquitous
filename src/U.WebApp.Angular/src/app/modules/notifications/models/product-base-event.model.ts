export interface ProductBaseEvent
{
  id: string;
  productId: string;
  name: string;
  creationDate: Date;
  manufacturer: string;
}
