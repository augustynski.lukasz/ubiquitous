export enum IntegrationEventType
{
  Unknown = 0,
  ProductPublishedIntegrationEvent = 1,
  ProductPropertiesChangedIntegrationEvent = 2,
  ProductAddedIntegrationEvent = 3
}
