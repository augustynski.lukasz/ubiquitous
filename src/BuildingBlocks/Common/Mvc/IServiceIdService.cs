namespace U.Common.Mvc
{
    public interface IServiceIdService
    {
         string Id { get; }
    }
}