namespace U.Common.Database
{
    public enum DbType
    {
        Unknown,
        Npgsql,
        Mssql
    }
}