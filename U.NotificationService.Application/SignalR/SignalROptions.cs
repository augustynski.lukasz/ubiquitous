namespace U.NotificationService.Application.SignalR
{
    public class SignalROptions
    {
        public string RedisConnectionString { get; set; }
    }
}